/*
 * rboot_custom_proc.h
 *
 *  Created on: Oct 29, 2017
 *      Author: PC
 */

#ifndef FIRMWARE_RBOOT_CUSTOM_PROC_H_
#define FIRMWARE_RBOOT_CUSTOM_PROC_H_

//save RTCMEM struct to RTCMEM DATA and update CRC
#define RTCMEM_SAVE_DATA(DATA_STRUCT)																			\
		{DATA_STRUCT.BlockCRC=CRC_Calc((u8 *) &DATA_STRUCT, sizeof(RTC_RBoot_Mem)-RTCMEM_CRC_SIZE);				\
		system_rtc_mem(RTCMEM_START_USER_BLOCK+RTCMEM_RBOOT_BLOCK, (void *)&DATA_STRUCT, sizeof(RTC_RBoot_Mem), RBOOT_RTC_WRITE);}		\



#endif /* FIRMWARE_RBOOT_CUSTOM_PROC_H_ */
